# [Section] Comments
# Single-line comment
"""  Multi-line comment """

print('Hello World')

# [Section] Variables
age = 35
middle_initial = "C"
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

# [Section] Data Types
# strings
full_name = "John Doe"
secret_code = 'Pa$$word'

# numbers
number_of_days = 365 # integer
pi_approx = 3.1416 # decimal/float
complex_num = 1 +5j # j represents imaginary number/component

# boolean
isLearning = True
isDifficult = False

# [Section] Using Variables
print("My name is " + full_name)
# print("My age is " + age)

# [Section] Typecasting
print("My age is " + str(age))
print("value: " + str(isLearning))
print(int(3.5))
print(int("9876"))
print(float(3))

# [Section] F-Strings
print(f"Hi! May name is {full_name}! My age is {age}.")

# Type Method
print(type(full_name))

# [Section] Operations
# Arithmetic
print(1 + 10)
print(1 - 10)
print(1 * 10)
print(1 / 10)
print(1 % 10)
print(1 ** 10)
print(1 // 10)

# Assignment Operator
num1 = 3
print(num1)

num1 += 4 # num1 = num1 + 4
print(num1)
num1 -= 4 # num1 = num1 - 4
print(num1)
num1 *= 4 # num1 = num1 * 4
print(num1)
num1 /= 4 # num1 = num1 / 4
print(num1)
num1 %= 4 # num1 = num1 % 4
print(num1)

# Comparison Operator
print(1 == 1) 
print(1 == "1")
print(1 != 1)
print(1 > 1)
print(1 < 1)
print(1 >= 1)
print(1 <= 1)

# Logical Operator
print(True and False)
print(not False)
print(True or False)


